#!/bin/zsh

echo "Configuring git."

# Config
git config --global status.submoduleSummary true
git config --global diff.submodule log

# Aliasenses
git config --global alias.dd "diff development..HEAD";
git config --global alias.l "log --oneline";

echo "alias blist=\"git branch > tmp-782349 && cat tmp-782349 && rm tmp-782349\"";