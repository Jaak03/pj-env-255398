#!/bin/zsh

echo "Configuring nginx."

# Configure php.
mkdir /tmp/php -p;
cd /tmp/php;
sed "s/\;cgi\.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/${PHP_VERSION}/cli/php.ini > ./php.ini \
  && mv ./php.ini /etc/php/${PHP_VERSION}/cli/php.ini;

# Configure FPM to listen over a TCP socker instead.
service php${PHP_VERSION}-fpm stop;

sed "s/listen = \/run\/php\/php${PHP_VERSION}-fpm.sock/listen = devcontainer:9000/g" /etc/php/${PHP_VERSION}/fpm/pool.d/www.conf > ./www.conf \
  && sudo mv ./www.conf /etc/php/${PHP_VERSION}/fpm/pool.d/;

service php${PHP_VERSION}-fpm start;