#!/bin/zsh

echo "Configuring environment."

# Configure sudo.
sed 's/\# \%wheel ALL=\(ALL\) NOPASSWD\: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers > /etc/sudoers.new \
  && export EDITOR="cp /etc/sudoers.new" \
  && visudo \
  && rm /etc/sudoers.new;
  
apt-get update -y;