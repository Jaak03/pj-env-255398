#!/bin/zsh

echo "Configuring GCP."

USER=$(whoami)

mkdir /home/${USER}/.config/gcloud -p 
chown -R ${USER} /home/${USER}/.config/gcloud
