import os
import docker
import sys

client = docker.from_env()

containers = client.containers.list()

SHELL_OPTIONS = [
  "zsh",
  "bash"
]

print("Which container would you like to open?")

i = 1
for container in containers:
  print(f"{i})\t{container.name}")
  i += 1

print(f"q)\tDo nothing")

print(f"\nEnter the container number [1-{i-1}]:")

answer = sys.stdin.readline()

def read_shell(path):
  shell = ''
  shell_file = None

  if not os.path.isdir(path):
    os.system(f"mkdir -p {path}")

    shell_file = open(f"{path}/SHELL", "w+")

    print(f"\nNo shell defined for this container yet, please choose one of the following shells ({SHELL_OPTIONS} = zsh): ", end="")

    shell_answer = (sys.stdin.readline()).strip()

    if shell_answer == "": shell_answer = "zsh"

    if shell_answer not in SHELL_OPTIONS:
      raise Exception("Did not recognize the chosen shell.")

    shell = shell_answer
    shell_file.write(shell_answer)
  else:
    print(f"Reading shell value from {path}.")
    shell_file = open(f"{path}/SHELL", "r")
    shell = shell_file.readline()

  shell_file.close()

  return shell

try:
  if answer == 'q\n': quit()
  else:
    answer = int(answer)
    if answer <= 0 and answer > i:
      raise Exception("Chosen option not in the range of containers.")
  
  path = f"{os.environ['HOME']}/.config/{containers[answer].name}"
  shell = read_shell(path)

  os.system(f"docker exec -it {containers[answer - 1].id} {shell}")
except Exception as e:
  print(e)

