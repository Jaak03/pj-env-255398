from tasks import arguments, permissions, cli, docker

# Clears all of the permissions in the repo.
permissions.open()

# Read and parse the command line arguments.
args = cli.retrieve_arguments()

# Store the content of the ARGS file into environment variables.
arguments.set_args()

# Up or rebuild the container based on the arguments.
if args.build:
  docker.build()
else:
  docker.up()