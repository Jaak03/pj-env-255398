import os

def open():
  """
  Sets the permissions for the whole repo to be completely open. This is done so that we don't run into permission issues later on in the script.
  """
  
  os.system("sudo chmod 777 ./ -R")