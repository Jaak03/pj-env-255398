import os

def up(daemon=True):
  """
  Starts the docker-compose file without rebuilding it.

  Args:
      daemon (bool, optional): This will typically not be used a lot, just in case you want to test the containers as the start and run. Defaults to True.
  """

  cmd = "docker-compose up"

  if daemon:
    cmd += " -d"

  os.system(cmd)

def build(daemon=True):
  """
  Builds and starts the container from scratch.

  Args:
      daemon (bool, optional): This will typically not be used a lot, just in case you want to test the containers as the start and run. Defaults to True.
  """
  
  cmd = "docker-compose up --build --remove-orphans"

  if daemon:
    cmd += " -d"

  os.system(cmd)