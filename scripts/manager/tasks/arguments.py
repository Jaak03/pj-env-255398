import os

def set_args():
  """
  Set the environment variables to be used as ARGS in docker.
  """
  with open("./scripts/arguments/environment", "r") as arg_file:
    args = arg_file.readlines()
    print("Setting system variables...")
    for arg in args:
      key, value = (lambda items : [item.rstrip("\n") for item in items])(arg.split("="))
      print(f"\t{key}={value}")
      os.environ[key] = value