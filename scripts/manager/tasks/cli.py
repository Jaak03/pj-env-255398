import argparse

def retrieve_arguments():
  """
  Reads and parses the command line arguments to be used in the command script.

  Returns:
    dict: args
  """
  
  # Create the parser
  my_parser = argparse.ArgumentParser(description='Basic command to manage starting the containers initially.')

  my_parser.add_argument(
    '-b',
    '--build',
    action='store_true',
    help='up the service with the --build flag'
  )

  return my_parser.parse_args()