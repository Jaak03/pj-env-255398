#!/bin/zsh

echo "Installing and configuring PHP ${PHP_VERSION}.";

# Add repo.
yes | add-apt-repository ppa:ondrej/php;
apt-get update -y;

# Setup basic PHP.
apt-get install -y php${PHP_VERSION};

# Setting up FPM related things.
a2enmod proxy_fcgi setenvif;
service apache2 restart;
a2enconf php7.2-fpm;
service apache2 reload;

# Install php${PHP_VERSION} and modules to have.
apt-get install -y php${PHP_VERSION}-cli;
apt-get install -y php${PHP_VERSION}-fpm;
apt-get install -y php${PHP_VERSION}-common;
apt-get install -y php${PHP_VERSION}-intl;
apt-get install -y php${PHP_VERSION}-mysql;
apt-get install -y php${PHP_VERSION}-mbstring;
apt-get install -y php${PHP_VERSION}-zip;
apt-get install -y php${PHP_VERSION}-memcached;
apt-get install -y php${PHP_VERSION}-soap;
apt-get install -y php${PHP_VERSION}-xml;
apt-get install -y php${PHP_VERSION}-xdebug;
apt-get install -y php${PHP_VERSION}-json;
apt-get install -y php${PHP_VERSION}-opcache;
apt-get install -y php${PHP_VERSION}-mcrypt;
apt-get install -y php${PHP_VERSION}-xml;
apt-get install -y php${PHP_VERSION}-curl;
apt-get install -y php${PHP_VERSION}-gd;

sh ./scripts/configure/php.sh;

echo "DONE"