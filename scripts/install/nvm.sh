#!/bin/zsh

echo "Installing NodeJS through NVM.";

# Downloading the source files.
export NVM_DIR=${HOME}/.nvm;
mkdir ${NVM_DIR} -p && cd ${NVM_DIR};
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash;

# Installing and configuring nvm.
. "${NVM_DIR}/nvm.sh" && nvm install ${NODE_VERSION} \
  && . "${NVM_DIR}/nvm.sh" && nvm use v${NODE_VERSION} \
  && . "${NVM_DIR}/nvm.sh" && nvm alias default v${NODE_VERSION} \
  && node --version \
  && npm --version \
  && npm config set user 0 \
  && npm config set unsafe-perm true \
  && sudo groupadd npm \
  && sudo usermod -aG npm ${DEVELOPER};

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

echo "DONE"