#!/bin/zsh

echo "Installing ngrok.";

mkdir /opt/ngrok -p;
cd /opt/ngrok;

wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip;

unzip *.zip && rm *.zip;

echo "alias ngrok=\"$(pwd)/ngrok\"" >> ~/.zshrc

echo "DONE"