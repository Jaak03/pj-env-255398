#!/bin/zsh

echo "Installing the AWS CLI.";

# Creating working directory for the downloaded files.
USER=$(whoami)

if [ $USER = "root" ];
then
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
    && unzip awscliv2.zip \
    && ./aws/install \
    && rm awscliv2.zip
else
echo '
===============================================
  ERROR: This script needs to be run as root.
===============================================
';
fi


echo "DONE"