#!/bin/zsh

echo "Installing oh-my-zsh.";

# Create directory to work in.
mkdir /tmp/ohmyzsh -p;
cd /tmp/ohmyzsh;

# Downloading repo.
export ZSH_HOME="/home/${DEVELOPER}/.oh-my-zsh";
yes | sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)";

cp ${ZSH_HOME}/templates/zshrc.zsh-template ~/.zshrc;
zsh;

echo "DONE";
