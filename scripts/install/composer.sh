#!/bin/zsh

echo "Installing composer.";

# Creating working directory for the downloaded files.
mkdir /tmp/composer -p
cd /tmp/composer

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

echo "DONE"