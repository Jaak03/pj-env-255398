#!/bin/zsh

echo "Installing further OS modules.";

apt-get install -y sudo \
  zsh \
  software-properties-common \
  apt-utils \
  build-essential \
  wget \
  git \
  unzip \
  curl \
  locales \
  make;

sh ./scripts/configure/environment.sh;

echo "DONE";
