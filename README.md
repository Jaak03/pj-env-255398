# PJ-ENV-255398 #

This is a combination of development environments.

## Launch container ##
The container depends on the UID and GID from your local and for some reason I could not get them into the script without you setting the variables on launch time.

Here are a list of the variables that need to be set:
1. USER_UID
2. USER_GID
3. DEVELOPER
4. DISPLAY
5. ROOT_PASSWORD
6. ROOT_PASSWORD

At the moment there is a very basic python script that automates setting these variables and you only have to run `/compose.py` and not worry about this for the most part.

## Git strategy ##
For this repo most of the configuration is kept in sync across the branches with only a few basic configuration changes between the environments. To do this I would like to use the following git strategy:
* Use the `main` branch for storing everything.
* This might change, but for now we then have `environment` branches. Change in the sense that there could be other branch-types added later. The format of the environment branches are:

        environment/[type]/[version]

To keep the main up to date we cherry-pick all of the relevant changes from the environment branch into main, and rebase onto that. What the graph should look like will then be:

        [environment/a/0.1] *   * [environment/b/1.7]
                             \ /
                              | 
                              * [main] 
## Known issues ##
* The GID variable is not set in all shells. In my case I use zsh which does have it set but when running my scripts with `bash` (or `sh` when bash is set as the default shell) it is not set and the Dockerfile fails to build.

    To fix this you can either manually export the GID variable before launching the script or change to a shell that does have this. [Here](https://www.howtoforge.com/linux-csh-command/) is an example of how to change your default shell.

    UPDATE: I changed the launch sript to run in zsh isntead. Not sure about how this will affect you if you do not have it installed so just keep that in mind.
* I want to really start using the cache for the updates and install files on the devcontainer. Found this about [cache-busting](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/) for the apt-get command and will try to incorporate that. This might mean that we sometimes get outdated packages and might have to include a no-cache somewhere. But for developing and speed this is they way to go I think.

## Notes ##
* To change the arguments for the dockerfile, including the DEVELOPER, you have to edit them in the `/scripts/manage/up` scripts.
* `DEBIAN_FRONTEND=noninteractive` is something you might have to set for some of the Dockerfile commands. 

## Future work ##
* In the compose script you can ask the user a few questions and then create a new docker-compose.yml file from scratch based on these questions. This will then only be done when the user uses a specific flag on build time.