FROM ubuntu:20.04

# Arguments coming from the docker-compose file. The assigned values are defualts.
ARG DISPLAY=:0
ARG USER_UID=1000
ARG USER_GID=1000
ARG DEVELOPER=developer
ARG HOME_DIR=/home/${DEVELOPER}
ARG ROOT_PASSWORD=password

# To stop it asking things...
ARG DEBIAN_FRONTEND=noninteractive

USER root

# Copy repo into working directory.
RUN mkdir /tmp/base/ -p
WORKDIR /tmp/base
COPY . .

# Change root password.
RUN yes ${ROOT_PASSWORD} | passwd root

# Update system
RUN apt-get update -y && apt-get upgrade -y

WORKDIR /tmp/base

# Install OS modules
RUN sh ./scripts/install/environment.sh

# Create the user for permission things
RUN echo "Configuring the "$DEVELOPER" user."
RUN groupadd -g ${USER_GID} ${DEVELOPER}
RUN mkdir ${HOME_DIR} -p
RUN useradd -d ${HOME_DIR} -s /bin/zsh --uid ${USER_UID} --gid ${USER_GID} ${DEVELOPER}
RUN yes ${ROOT_PASSWORD} | passwd ${DEVELOPER}
RUN usermod -aG sudo ${DEVELOPER}

# Directory permissions
RUN chown -R ${DEVELOPER} ${HOME_DIR}
RUN chmod 777 /tmp /opt -R

# Install requirements for the laravel framework.
RUN sh ./scripts/install/php71.sh
RUN sh ./scripts/install/composer.sh

# Installing GCP
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y
RUN mkdir /home/${DEVELOPER}/.config/gcloud -p && chown -R ${DEVELOPER} /home/${DEVELOPER}/.config/gcloud

# Changing home directory permissions one last time.
RUN chown ${DEVELOPER}:${DEVELOPER} ${HOME_DIR} -R

USER ${DEVELOPER}
RUN sh ./scripts/install/laravel.sh


# Configure the shell.
RUN sh ./scripts/install/ohmyzsh.sh

# Getting things ready for when you ssh into the container.
USER ${DEVELOPER}
WORKDIR ${HOME_DIR}
