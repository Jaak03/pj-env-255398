FROM ubuntu:20.04

# Arguments coming from the docker-compose file. The assigned values are defualts.
ARG DISPLAY=:0
ARG USER_UID=1000
ARG USER_GID=1000
ARG DEVELOPER=developer
ARG ROOT_PASSWORD=password
ARG REACT_VERSION=17.0.2
ARG NODE_VERSION=15.2.1

USER root

ENV HOME_DIR=/home/${DEVELOPER}

# Copy repo into working directory.
RUN mkdir /tmp/base/ -p
WORKDIR /tmp/base
COPY . .

# Change root password.
RUN yes ${ROOT_PASSWORD} | passwd root

# Update system. Split the command to make use of caching, see cache-busting in the readme.
RUN echo 'Upgrading' && apt-get update -y --fix-missing && apt-get upgrade -y
RUN apt-get update -y --fix-missing && apt-get install -y zsh sudo

# Configure sudo permissions
RUN sh ./scripts/configure/environment.sh

WORKDIR /tmp/base

# Create the user for permission things
RUN echo "Configuring the "$DEVELOPER" user."
RUN groupadd -g ${USER_GID} ${DEVELOPER}
RUN useradd -s /bin/zsh --uid ${USER_UID} --gid ${USER_GID} ${DEVELOPER} -m
RUN yes ${ROOT_PASSWORD} | passwd ${DEVELOPER}
RUN usermod -aG sudo ${DEVELOPER}

# Install OS modules
RUN echo "Installing further OS modules.";

RUN apt-get update -y --fix-missing && DEBIAN_FRONTEND=noninteractive apt-get install -y wget \
  git \
  software-properties-common \
  apt-utils \
  build-essential \
  zip \
  curl

# Directory permissions
RUN chmod 777 /tmp /opt -R

USER ${DEVELOPER}

# Configure the shell.
RUN sh ./scripts/install/ohmyzsh.sh

# Getting things ready for when you ssh into the container.
WORKDIR ${HOME_DIR}